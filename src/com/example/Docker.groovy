#!/user/bin/ env groovy
package com.example

class  Docker implements Serializable{
    def script

    Docker (script){
        this.script = script
    }

    def versionIncreament(){
    script.sh 'mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit'
        def matcher = script.readFile('pom.xml')=~ '<version>(.+)</version>'
        def version = matcher[0][1]
        script.env.IMAGE_NAME = "$script.BUILD_NAME:$version-${script.BUILD_NUMBER}"
        }


    def buildDockerImage(String imageName) {
        script.echo "building the image..."
        script.sh "docker build -t $imageName ."
        }

    def dockerLogin() {
        script.echo "Logging in to docker environment..."
        script.withCredentials([script.usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
            script.sh "echo '${script.PASS}' | docker login -u '${script.USER}' --password-stdin"
        }
    }

    def dockerPush(String imageName){
        script.sh "docker push $imageName"
    }

    def deployToEc2(){
        def shellCmd = "bash ./server-cmds.sh ${script.IMAGE_NAME}"
        def ec2Instance = "ec2-user@18.209.17.77"
  
        script.sshagent(['ec2-server-key']) {
        script.sh "scp server-cmds.sh ${ec2Instance}:/home/ec2-user"
        script.sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
        script.sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
        }
    }


    def commitWork(){
        script.withCredentials([script.usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
            script.sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/muheezopebiyi/java-maven-app.git'   
            script.sh 'git config --global user.email "moe1@example.com"'
            script.sh 'git config --global user.name "moe1"'
            script.sh 'git add .'
            script.sh 'git commit -m "ci: version bump"'
            script.sh 'git push origin HEAD:jenkins-jobs'
        }
    }
}

